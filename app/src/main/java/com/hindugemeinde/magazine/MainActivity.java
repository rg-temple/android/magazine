package com.hindugemeinde.magazine;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageReference;
import com.hindugemeinde.magazine.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;

    private StorageReference mStorageRef;

    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        mStorageRef = FirebaseStorage.getInstance().getReference();
        relativeLayout = (RelativeLayout) findViewById(R.id.MainLayout);

        StorageReference DownloadRef = mStorageRef.child("NewsLetter_25_Dec_2020.pdf");

        String[][] list = getFileList(DownloadRef, 1);
        Log.i(TAG, "onCreate: Magazine list: " + list[0][0]);


        // TODO: Add a list of files and change to button
        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (internetIsConnected()) {
                    Snackbar.make(view, "Downloading magazine", Snackbar.LENGTH_LONG).show();
                    getFiles(DownloadRef, "NewsLetter_25_Dec_2020.pdf");
                } else {
                    Snackbar.make(view, "Error: internet not connected", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    // FIXME: Useful link: https://www.youtube.com/watch?v=3D7M0zXwY3s
    public String[][] getFileList(StorageReference listRef, int read) {
        String ret[][] = new String[2][read];

        listRef.listAll()
                .addOnSuccessListener(new OnSuccessListener<ListResult>() {
                    @Override
                    public void onSuccess(ListResult listResult) {
                        String prefixes[] = new String[read];
                        int size = 0;
                        for (StorageReference prefix : listResult.getPrefixes()) {
                            // All the prefixes under listRef.
                            // You may call listAll() recursively on them.
                            prefixes[size] = prefix.toString();
                            if (size == 10) {
                                break;
                            }
                            size++;
                        }

                        String suffixes[] = new String[read];
                        size = 0;

                        for (StorageReference item : listResult.getItems()) {
                            // All the items under listRef.
                            suffixes[size] = item.toString();
                            if (size == 10) {
                                break;
                            }
                            size++;
                        }
                        ret[0] = suffixes;
                        ret[1] = prefixes;
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar.make(relativeLayout, "Failed to read from Database", Snackbar.LENGTH_LONG)
                                .setAction(R.string.try_again, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        getFileList(listRef, read);
                                    }
                                })
                                .show();
                    }
                });
        return ret;
    }

    public boolean internetIsConnected() {
        try {
            String command = "ping -c 1 google.com";
            return (Runtime.getRuntime().exec(command).waitFor() == 0);
        } catch (Exception e) {
            return false;
        }
    }

    public void getFiles(StorageReference ref, String file) {
        ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                _download(MainActivity.this, file, DIRECTORY_DOWNLOADS, uri.toString());
                Snackbar.make(relativeLayout, "Successfully downloaded Magazine. Saved at: "
                                + DIRECTORY_DOWNLOADS, Snackbar.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Snackbar.make(relativeLayout, "Failed downloaded Magazine",
                        Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public void _download(Context context, String file, String destination, String url) {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationInExternalFilesDir(context, destination, file);
        downloadManager.enqueue(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }
}